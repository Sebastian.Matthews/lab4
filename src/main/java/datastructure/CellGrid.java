package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState[][] cellState;
    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        cellState = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= numRows()||column < 0||column >=numColumns()) {
            throw new IndexOutOfBoundsException("incorrect values");
        }
        cellState[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if ((row < 0) || (row >= numRows())||(column < 0)||(column >=numColumns())) {
            throw new IndexOutOfBoundsException("incorrect values");
        }
        return cellState[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newCellGrid = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                CellState cellState = get(i,j);
                newCellGrid.set(i, j, cellState);
            }
        }
        return newCellGrid;
    }
    
}
